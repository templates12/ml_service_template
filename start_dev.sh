#!/usr/bin/env bash
set -e
mkdir -p cert

docker-compose down
docker-compose build

docker-compose run app bash -c 'bash -s <<EOF
service_manage drop_db
service_manage create_db
service_manage add_user
service_manage show_db
EOF'

#docker-compose up -d --force-recreate
docker-compose up -d
docker-compose logs -f
