import flask
import flask_httpauth
import flask_migrate
import flask_sqlalchemy

from sqlalchemy.ext.declarative import declarative_base
from werkzeug import security
import itsdangerous
from expiringdict import ExpiringDict

from mlservice import config


app = flask.Flask(__name__)
app.config.from_pyfile('config.py')

basic_auth = flask_httpauth.HTTPBasicAuth()
token_auth = flask_httpauth.HTTPTokenAuth()
db = flask_sqlalchemy.SQLAlchemy(app)
migrate = flask_migrate.Migrate(app, db, directory='service/migrations')

jwt = itsdangerous.TimedJSONWebSignatureSerializer(config.SECRET_KEY, expires_in=config.TOKEN_EXPIRES)
Base = declarative_base()


Models_cache = ExpiringDict(max_len=5, max_age_seconds=60*60)
