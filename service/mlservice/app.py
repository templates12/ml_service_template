import sys
import os
import multiprocessing
import glob

import click
import flask
from flask import request
from flask import json
from werkzeug import security
import itsdangerous

from mlservice.instances import app
from mlservice.instances import basic_auth
from mlservice.instances import token_auth

from mlservice import config
from mlservice import models
from mlservice import helpers
from mlservice.logger import logger


def get_current_user():
    return getattr(flask.g, 'user', None)


@basic_auth.verify_password
def verify_password(username, password):
    flask.g.user = None
    user = models.User.query.filter_by(username=username).first()
    if not user or not user.verify_password(password):
        return False
    flask.g.user = user
    return True


@token_auth.verify_token
def verify_token(token):
    flask.g.user = None
    user = models.User.verify_auth_token(token)
    if not user:
        return False
    flask.g.user = user
    return True


# ----------------------------------------------------------
# ------------------ RPC -----------------------------------
# ----------------------------------------------------------
@app.route('/token', methods=['POST'])
# @basic_auth.login_required  # not required any more due to POST & verify_password
def token():
    logger.info('SQLLITE: {}'.format(config.SQLALCHEMY_DATABASE_URI))
    path = "{}".format(config.SQLALCHEMY_DATABASE_URI)[10:]
    logger.info('FILE DB: {}'.format(glob.glob("/service/*.db")))
    logger.info('FILE DB: {}'.format(os.listdir("/service/")))
    logger.info('FILE DB: {}'.format(os.listdir("/service/mlservice/")))    
    jdata = request.get_json()
    logger.info("request json data = {}".format(jdata))
    if not jdata:
        return json.dumps({'error': 'login'}), 402
    user, passwd = jdata.get('user'), jdata.get('passwd')
    logger.info("request user = {}; passwd={}".format(user, passwd))
    logger.info("all users={}".format(models.User.query.all()))
    if not verify_password(user, passwd):
        return json.dumps({'error': 'login'}), 406
    token = flask.g.user.generate_auth_token()
    return json.dumps({'token': token.decode('ascii'), 'duration': config.TOKEN_EXPIRES}), 200


@app.route('/check_token', methods=['POST'])
@token_auth.login_required
def check_token():
    return json.dumps({'message': 'token is valid'}), 200


@app.route('/ver', methods=['POST'])
def version():
    return json.dumps({'ver': '1.2.3'}), 200


@app.route('/model', methods=['POST'])
@token_auth.login_required
def run_model():
    #p1 = multiprocessing.Process(target=helpers.run_mnist)
    #p1.start()
    #p1.join()
    #return json.dumps({'result': 'OK'}), 200    
    dlmodel = helpers.DLModel()
    p = dlmodel.run(model_name='mnist')
    logger.info("res = {}".format(p))
    return json.dumps({'result': p}), 200


# ----------------------------------------------------------
# ------------------ MAIN ----------------------------------
# ----------------------------------------------------------
@click.command()
@click.option('--debug', '-d', is_flag=True, help='Enable debug mode')
@click.option('--port', '-p', type=str, help='Enable debug mode')
def main(debug, port):
    app.run(host='0.0.0.0', port=port, debug=debug)


if __name__ == '__main__':  # pragma: no cover
    main()
