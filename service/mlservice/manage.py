import datetime
import flask_script
import flask_migrate

from mlservice import models
from mlservice import config
from mlservice.instances import app
from mlservice.instances import db


manager = flask_script.Manager(app)

# migrations
# manager.add_command('db', flask_migrate.MigrateCommand)

@manager.command
def drop_db():
    """Drops the db tables."""
    print(config.SQLALCHEMY_DATABASE_URI)
    print(db)
    db.reflect()
    db.drop_all()


@manager.command
def create_db():
    """Create the db tables."""
    print(config.SQLALCHEMY_DATABASE_URI)
    print(db)
    db.create_all()


@manager.command
def add_user(login='user', passwd='123'):
    print(config.SQLALCHEMY_DATABASE_URI)
    print(db)
    user = models.User(username=login, password=passwd)
    db.session.add(user)
    db.session.commit()

@manager.command
def show_db():
    """Create the db tables."""
    users = models.User.query.all()
    print(users)
    print(config.SQLALCHEMY_DATABASE_URI)

def main():
    manager.run()


if __name__ == '__main__':  # pragma: no cover
    main()
