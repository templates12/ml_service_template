import datetime as dt
import uuid

import sqlalchemy
from sqlalchemy.ext import declarative
from werkzeug import security
import itsdangerous

from mlservice.instances import db
from mlservice.instances import Base
from mlservice.instances import jwt
from mlservice.instances import migrate


# ----------------------------------------------------------
# ------------------ BASE MODEL ----------------------------
# ----------------------------------------------------------
class ModelSerializerMixin(object):
    __table_args__ = {'extend_existing': True}

    READONLY_FIELDS = ()
    HIDDEN_FIELDS = ()
    FIELD_TYPES = {}

    @classmethod
    def from_dict(cls, d):
        obj = cls()
        for key, value in d.items():
            if key in cls.READONLY_FIELDS:
                flask_restful.abort(
                    400, message='{!r} is read-only'.format(key))
            setattr(obj, key, value)
        return obj

    def update_from_dict(self, d):
        for key, value in d.items():
            if key in self.READONLY_FIELDS:
                flask_restful.abort(
                    400, message='{!r} is read-only'.format(key))
            setattr(self, key, value)

    def to_dict(self):
        d = {}
        for column in self.__table__.columns:
            if column.name in self.HIDDEN_FIELDS:
                continue
            attr = getattr(self, column.name)
            if isinstance(attr, dt.datetime):
                attr = attr.isoformat()
            d[column.name] = attr
        return d

    @classmethod
    def get_fields_parser(cls):
        parser = reqparse.RequestParser()
        for column in cls.__table__.columns:
            col_name = column.name
            if col_name in cls.HIDDEN_FIELDS:
                continue
            if col_name in cls.FIELD_TYPES:
                col_type = cls.FIELD_TYPES[col_name]
            else:
                try:
                    col_type = getattr(column.type, 'python_type')
                except NotImplementedError:
                    continue
            if col_type:
                parser.add_argument(col_name, type=col_type,
                                    store_missing=False, location='json')
                if col_type is not list:
                    parser.add_argument(col_name, type=col_type,
                                        store_missing=False, location='args')
        return parser


class CRUDMixin(object):
    __table_args__ = {'extend_existing': True}

    @classmethod
    def create(cls, **kwargs):
        instance = cls(**kwargs)
        return instance.save()

    @classmethod
    def get_or_create(cls, **kwargs):
        instance = cls.query.filter_by(**kwargs).one_or_none()
        if not instance:
            instance = cls.create(**kwargs)
        return instance

    @classmethod
    def get_or_none(cls, **kwargs):
        instance = cls.query.filter_by(**kwargs).one_or_none()
        return instance

    def update(self, commit=True, **kwargs):
        for attr, value in kwargs.iteritems():
            setattr(self, attr, value)
        return commit and self.save() or self

    def save(self, commit=True):
        db.session.add(self)
        if commit:
            db.session.commit()
        return self

    def delete(self, commit=True):
        db.session.delete(self)
        return commit and db.session.commit()

    def refresh(self):
        db.session.refresh(self)


class BaseModel(db.Model, Base, CRUDMixin, ModelSerializerMixin):
    __abstract__ = True

    id = db.Column(db.Integer, primary_key=True)
    created_on = db.Column(db.DateTime, default=dt.datetime.utcnow)
    updated_on = db.Column(db.DateTime, default=dt.datetime.utcnow,
                           onupdate=dt.datetime.utcnow)

    @declarative.declared_attr
    def __tablename__(cls):
        return cls.__name__.lower()


class User(BaseModel):
    username = db.Column(db.String(128), unique=True, nullable=False)
    password_hash = db.Column(db.String(256), nullable=False)


    @property
    def password(self):
        return None

    @password.setter
    def password(self, password):
        self.password_hash = security.generate_password_hash(password)

    def verify_password(self, password):
        return security.check_password_hash(self.password_hash, password)

    def generate_auth_token(self):
        return jwt.dumps({'id': self.id})

    @staticmethod
    def verify_auth_token(token):
        try:
            data = jwt.loads(token)
        except itsdangerous.SignatureExpired:
            return None    # valid token, but expired
        except itsdangerous.BadSignature:
            return None    # invalid token
        user = User.query.get(data['id'])
        return user

    def __str__(self):
        return self.username

    def __unicode__(self):
        return self.username
