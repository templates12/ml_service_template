import sys
import numpy as np
import tensorflow
import keras
from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing import image
from tensorflow.keras.preprocessing.image import img_to_array, load_img
from mlservice.instances import Models_cache
# how to use: python3 service/helpers.py mnist service/models/img_15.jpg service/models/cnn2.h5

class DLModel():

    def run(self, *args, **kargs):
        model_name = kargs.get('model_name')
        if  hasattr(self.__class__, model_name) and callable(getattr(self.__class__, model_name)):
            method = getattr(self.__class__, model_name)
            return method(self, *args, **kargs)
        return None

    def mnist(self, *args, **kargs):
        print("KERAS_VER={}".format(keras.__version__))
        print("TENSOR_VER={}".format(tensorflow.__version__))
        mldata = kargs.get('mldata', '/service/models/img_15.jpg')
        modelpath = kargs.get('modelpath', '/service/models/cnn2.h5')
        filepath = mldata
        if not Models_cache.get('mnist'):
            Models_cache['mnist'] = load_model(modelpath)
        test_model = Models_cache['mnist']
        img_width, img_height = 28, 28
        img = load_img(filepath, False, target_size=(img_width,img_height))
        img2 = np.mean(img, axis=2)
        x = img_to_array(img2)
        print(x.shape) # (28,28)
        x1 = np.expand_dims(x, axis=0)
        x_test = x1
        #x_test = x1.reshape(28, 28, 1)
        preds = test_model.predict(x_test)
        print("preds = {}".format(preds))
        prob = None
        res = [i for i in range(0, len(preds[0])) if preds[0][i] > 0.9]
        if res:
            res = res[0]
        else:
            res = None
        #preds = test_model.predict_classes(x_test)
        #prob = test_model.predict_proba(x_test)
        return res


if __name__ == '__main__':  # pragma: no cover
    dlmodel = DLModel()
    res = dlmodel.run(model_name=sys.argv[1], mldata=sys.argv[2], modelpath=sys.argv[3])
    print("res = {}".format(res))
