import os

# flask configuration
SECRET_KEY = 'default_secrete'
SECURITY_PASSWORD_SALT = 'salt_passwd_look'
SQLALCHEMY_DATABASE_URI = os.environ.get('SQLALCHEMY_DATABASE_URI', 'sqlite:///service.db')
SQLALCHEMY_TRACK_MODIFICATIONS = False

# application configuration
TOKEN_EXPIRES = 3600
