import os

try:  # for pip >= 10
    from pip._internal.req import parse_requirements
except ImportError:  # for pip <= 9.0.3
    from pip.req import parse_requirements

import setuptools

setup_dir = os.path.dirname(os.path.realpath(__file__))
install_reqs = parse_requirements(os.path.join(setup_dir, 'requirements.txt'),
                                  session=False)
reqs = [str(ir.req) for ir in install_reqs]

setuptools.setup(
    install_requires=reqs
)
